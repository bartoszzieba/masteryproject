#!/bin/bash
SET_TYPE="$1"
SIMULATION_TYPE="$2"
SIMULATION_OUT_DIR="$3"
EXTRA_VARS="$4"
JVM_ARGS="$5"
SIMULATION_NUM="$6"

for SIM in $(seq -w 1 $SIMULATION_NUM)
do
	echo "Simulation $SIM"
	SIM_DIR="$SIMULATION_OUT_DIR/$SIM"
	mkdir -p $SIM_DIR

	for GC_SWITCH in "-XX:+UseConcMarkSweepGC" "-XX:+UseParallelGC" "-XX:+UseParallelOldGC" "-XX:+UseG1GC" "-XX:+UseSerialGC"
	do
		GC_NAME=$(echo "$GC_SWITCH" | sed "s/^.*Use//") 
		echo $GC_SWITCH

		SIMULATION_PATH="$SIM_DIR/$GC_NAME"
		mkdir -p $SIMULATION_PATH
	
		echo "java $GC_SWITCH $JVM_ARGS -jar sets-all.jar -m \"$SIMULATION_PATH/jvm\" -o $SIMULATION_PATH -s $SET_TYPE -t $SIMULATION_TYPE $EXTRA_VARS" > "$SIMULATION_PATH/invoke.txt"
		java $GC_SWITCH $JVM_ARGS -jar sets-all.jar -m "$SIMULATION_PATH/jvm" -o $SIMULATION_PATH -s $SET_TYPE -t $SIMULATION_TYPE $EXTRA_VARS 2>&1 1>"$SIMULATION_PATH/stdout_stderr"
		
		find $SIMULATION_PATH -type f | xargs gzip 
	done
done
