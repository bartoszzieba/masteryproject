#!/bin/bash

IS_SWAP_ENABLE=$(swapon | grep "zram")

if [ -z "$IS_SWAP_ENABLE" ]
then
	modprobe zram
	ZRAM=""
	echo "4G" > /sys/block/zram0/disksize
	PARTITION="/dev/zram0"
	mkswap $PARTITION
	swapon $PARTITION
fi
