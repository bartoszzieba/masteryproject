#!/bin/bash
JAR=$1
JAR2=$2
CSV_SORTED_BY_TIME=$3
CSV_SORTED_BY_SIZE=$4
JVM_METRICS=$5
OUT_DIR=$6
SIZE_BIND=${7-1000}

java -jar $JAR -a JVM_MEMORY -i $JVM_METRICS -o $OUT_DIR/jvm-memory.csv
java -jar $JAR -a JVM_GC -i $JVM_METRICS -o $OUT_DIR/jvm-gc.csv

java -jar $JAR -a CSV_BY_TIMESTAMP_COUNTER -i $CSV_SORTED_BY_TIME -o $OUT_DIR/count-per-time.csv
java -jar $JAR -a CSV_TIME_EXECUTION_BY_SIZE -i $CSV_SORTED_BY_SIZE -o $OUT_DIR/time-execution-per-size.csv

java -jar $JAR2 -i $OUT_DIR/jvm-memory.csv -o $OUT_DIR/jvm-memory.png -k TO_SEC -v TO_MB -t "Użycie pamięci" -x "czas [s]" -y "pamięć [mb]"
java -jar $JAR2 -i $OUT_DIR/jvm-gc.csv -o $OUT_DIR/jvm-gc.png -k TO_SEC -v TO_SEC -t "Użycie Garbage Collectora" -x "czas [s]" -y "użycie GC[s]" -a
java -jar $JAR2 -i $OUT_DIR/count-per-time.csv -o $OUT_DIR/count-per-time.png -k TO_SEC -t "Ilość operacji na sekundę" -x "czas [s]" -y "ilość operacji"
java -jar $JAR2 -i $OUT_DIR/time-execution-per-size.csv -o $OUT_DIR/time-execution-per-size.png -t "Czas trwania operacji w zależności od rozmiaru zbioru" -x "rozmiar" -y "czas [ns]" -m $SIZE_BIND

