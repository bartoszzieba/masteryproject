#!/bin/bash
DATA_DIRECOTRY=$1

gzip -d $DATA_DIRECOTRY/*.gz

sort -t "," -k 5 -n -m $DATA_DIRECOTRY/*.log > $DATA_DIRECOTRY/sortedByTimestamp.csv

sort -t "," -k 2 -n $DATA_DIRECOTRY/sortedByTimestamp.csv > $DATA_DIRECOTRY/sortedBySize.csv

mkdir $DATA_DIRECOTRY/results

tail -n 1 $DATA_DIRECOTRY/sortedByTimestamp.csv > $DATA_DIRECOTRY/results/simulationTime.txt

./runAgregatorAndChartsOnData.sh agregator-all.jar charts-all.jar $DATA_DIRECOTRY/sortedByTimestamp.csv $DATA_DIRECOTRY/sortedBySize.csv $DATA_DIRECOTRY/jvm $DATA_DIRECOTRY/results 10000

rm $DATA_DIRECOTRY/sortedByTimestamp.csv $DATA_DIRECOTRY/sortedBySize.csv

gzip $DATA_DIRECOTRY/*

