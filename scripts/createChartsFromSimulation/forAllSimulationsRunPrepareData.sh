#!/bin/bash
SIM_DIR=$1
if [ -z "$SIM_DIR" ] 
then
	echo "As a prameter enter a simulation directory"
fi

for DIR in $(find $SIM_DIR -type d | grep GC$ | sort)
do
	echo $DIR
	./prepareDataAndRunAgregator.sh $DIR
done
