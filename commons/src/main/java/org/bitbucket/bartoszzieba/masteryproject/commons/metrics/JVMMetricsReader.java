package org.bitbucket.bartoszzieba.masteryproject.commons.metrics;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.util.Arrays;
import java.util.List;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMGarbageCollectorMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetrics;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;

public class JVMMetricsReader {

	private final List<MemoryPoolMXBean> memoryPoolMXBeans;
	private final List<GarbageCollectorMXBean> garbageCollectorMXBeans;
	private final MemoryMXBean memoryMXBean;

	public JVMMetricsReader() {
		memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
		garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
		memoryMXBean = ManagementFactory.getMemoryMXBean();
	}

	public JVMMetrics getActualJVMMetrics() {
		JVMMetrics metrics = new JVMMetrics();
		metrics.memory_metrics = getMemoryInformations();
		metrics.garbage_collestors_metric = getGarbageCollectorMetrics();
		return metrics;
	}

	private JVMMemoryMetrics getMemoryInformations() {
		JVMMemoryMetrics memoryMetrics = new JVMMemoryMetrics();

		memoryMetrics.heap_memory = copyMemoryUsage(memoryMXBean.getHeapMemoryUsage(), "HEAP_MEMORY");
		memoryMetrics.non_heap_memory = copyMemoryUsage(memoryMXBean.getNonHeapMemoryUsage(), "NON_HEAP_MEMORY");

		memoryMetrics.heap_pools_memory = memoryPoolMXBeans.stream().map(this::copyMemoryUsage).collect(toList());
		return memoryMetrics;
	}

	private JVMMemoryMetric copyMemoryUsage(MemoryPoolMXBean memoryPool) {
		return copyMemoryUsage(memoryPool.getUsage(), memoryPool.getName());
	}

	private JVMMemoryMetric copyMemoryUsage(MemoryUsage memoryUsage, String name) {
		JVMMemoryMetric memoryMetric = new JVMMemoryMetric();
		memoryMetric.name = name;

		memoryMetric.commited = memoryUsage.getCommitted();
		memoryMetric.max = memoryUsage.getMax();
		memoryMetric.used = memoryUsage.getUsed();
		return memoryMetric;
	}

	private List<JVMGarbageCollectorMetric> getGarbageCollectorMetrics() {
		return garbageCollectorMXBeans.stream().map(this::copyGarbageCollectionMetric).collect(toList());
	}

	private JVMGarbageCollectorMetric copyGarbageCollectionMetric(GarbageCollectorMXBean garbageCollectorMXBean) {
		JVMGarbageCollectorMetric garbageCollectorMetric = new JVMGarbageCollectorMetric();

		garbageCollectorMetric.name = garbageCollectorMXBean.getName();
		garbageCollectorMetric.memory_pools = Arrays.stream(garbageCollectorMXBean.getMemoryPoolNames())
				.collect(joining(","));

		garbageCollectorMetric.collection_count = garbageCollectorMXBean.getCollectionCount();
		garbageCollectorMetric.collection_time = garbageCollectorMXBean.getCollectionTime();
		return garbageCollectorMetric;
	}

}
