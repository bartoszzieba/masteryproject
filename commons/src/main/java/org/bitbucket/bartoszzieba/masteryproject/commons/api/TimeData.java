package org.bitbucket.bartoszzieba.masteryproject.commons.api;

public class TimeData extends TimestampExtension {

	public long time_start_nanos = System.nanoTime();
	public long time_end_nanos;

	public void updateEndNanos() {
		time_end_nanos = System.nanoTime();
	}

}
