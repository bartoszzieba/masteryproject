package org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm;

import java.util.List;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.TimestampExtension;

public class JVMMetrics extends TimestampExtension {

	public JVMMemoryMetrics memory_metrics;
	public List<JVMGarbageCollectorMetric> garbage_collestors_metric;

}
