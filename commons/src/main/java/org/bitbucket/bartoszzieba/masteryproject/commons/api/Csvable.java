package org.bitbucket.bartoszzieba.masteryproject.commons.api;

public interface Csvable {

	public String getSchema();

	public String toCsvLine();

}
