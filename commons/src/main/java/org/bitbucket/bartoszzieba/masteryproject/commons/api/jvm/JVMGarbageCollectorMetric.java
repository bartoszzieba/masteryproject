package org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm;

public class JVMGarbageCollectorMetric {

	public String name;
	public String memory_pools;

	public long collection_count;
	public long collection_time;

}
