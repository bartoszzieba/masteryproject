package org.bitbucket.bartoszzieba.masteryproject.commons.logger;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UncheckedIOException;

abstract public class SimulationLogger implements AutoCloseable {

	private final PrintStream loggerOut;

	protected SimulationLogger(String fileName) {
		this(createPrintStream(fileName));
	}

	private static PrintStream createPrintStream(String fileName) {
		try {
			return new PrintStream(fileName);
		} catch (FileNotFoundException e) {
			throw new UncheckedIOException(e);
		}
	}

	protected SimulationLogger(PrintStream loggerOut) {
		this.loggerOut = loggerOut;
	}

	protected void writeLine(String msg) {
		loggerOut.println(msg);
	}

	@Override
	public void close() {
		loggerOut.flush();
		loggerOut.close();
	}

}
