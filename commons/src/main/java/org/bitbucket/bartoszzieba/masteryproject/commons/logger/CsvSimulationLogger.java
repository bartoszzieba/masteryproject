package org.bitbucket.bartoszzieba.masteryproject.commons.logger;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.Csvable;

public class CsvSimulationLogger<T extends Csvable> extends SimulationLogger {

	public CsvSimulationLogger(String fileName) {
		super(fileName);
	}

	public void writeElement(T csvalbe) {
		writeLine(csvalbe.toCsvLine());
	}

}
