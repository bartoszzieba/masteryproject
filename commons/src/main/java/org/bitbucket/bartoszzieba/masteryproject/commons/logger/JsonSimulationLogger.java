package org.bitbucket.bartoszzieba.masteryproject.commons.logger;

import java.io.PrintStream;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.Gsonable;

import com.google.gson.Gson;

public class JsonSimulationLogger extends SimulationLogger {

	private final Gson gson = new Gson();

	public JsonSimulationLogger(String fileName) {
		super(fileName);
	}

	public JsonSimulationLogger(PrintStream loggerOut) {
		super(loggerOut);
	}

	public void writeGsonable(Gsonable object) {
		writeLine(gson.toJson(object));
	}

}
