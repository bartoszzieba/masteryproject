package org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm;

import java.util.List;

public class JVMMemoryMetrics {

	public JVMMemoryMetric heap_memory;
	public JVMMemoryMetric non_heap_memory;

	public List<JVMMemoryMetric> heap_pools_memory;

}
