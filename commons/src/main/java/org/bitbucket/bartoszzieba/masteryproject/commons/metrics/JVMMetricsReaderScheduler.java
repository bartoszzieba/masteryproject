package org.bitbucket.bartoszzieba.masteryproject.commons.metrics;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;
import org.bitbucket.bartoszzieba.masteryproject.commons.logger.JsonSimulationLogger;

public class JVMMetricsReaderScheduler {

	private final JVMMetricsReader jvmMetricsReader;
	private final long millisInterval;
	private final JsonSimulationLogger logger;
	private ScheduledExecutorService scheduler = null;

	public JVMMetricsReaderScheduler(long millisInterval, JsonSimulationLogger logger) {
		this.millisInterval = millisInterval;
		this.logger = logger;
		jvmMetricsReader = new JVMMetricsReader();
	}

	public void start() {
		if (scheduler != null) {
			throw new IllegalStateException("This instance is running");
		}

		scheduler = Executors.newScheduledThreadPool(1);

		scheduler.scheduleWithFixedDelay(this::taskWritesJVMMetricsToLogger, 0, millisInterval, TimeUnit.MILLISECONDS);
	}

	public void stop() {
		if (scheduler == null) {
			throw new IllegalStateException("This instance was stopped or newer started");
		}

		scheduler.shutdown();
		scheduler = null;
	}

	private void taskWritesJVMMetricsToLogger() {
		JVMMetrics actualJVMMetrics = jvmMetricsReader.getActualJVMMetrics();
		logger.writeGsonable(actualJVMMetrics);
	}

}
