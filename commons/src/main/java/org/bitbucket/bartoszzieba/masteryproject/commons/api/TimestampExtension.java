package org.bitbucket.bartoszzieba.masteryproject.commons.api;

public abstract class TimestampExtension implements Gsonable {

	public static long APPLICATION_START_TIME = 0;

	public long timestamp = System.currentTimeMillis() - APPLICATION_START_TIME;

}
