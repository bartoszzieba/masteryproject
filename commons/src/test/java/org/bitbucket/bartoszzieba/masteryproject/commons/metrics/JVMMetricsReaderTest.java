package org.bitbucket.bartoszzieba.masteryproject.commons.metrics;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMGarbageCollectorMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetrics;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;
import org.junit.Test;

public class JVMMetricsReaderTest {

	@Test
	public void shouldReturnNonEmptyMetric() {
		JVMMetricsReader metricsReader = new JVMMetricsReader();

		JVMMetrics metrics = metricsReader.getActualJVMMetrics();

		assertThat(metrics).isNotNull();

		checkGarbageCollectorMetrics(metrics.garbage_collestors_metric);

		checkMemoryMetrics(metrics.memory_metrics);
	}

	private void checkGarbageCollectorMetrics(List<JVMGarbageCollectorMetric> gcMetrics) {
		assertThat(gcMetrics).isNotEmpty();
		assertThat(gcMetrics).doesNotHaveDuplicates();
		assertThat(gcMetrics).allSatisfy(metric -> {
			assertThat(metric).isNotNull();
			assertThat(metric.name).isNotEmpty();
			assertThat(metric.memory_pools).isNotEmpty();
			assertThat(metric.collection_count).isNotNegative();
			assertThat(metric.collection_time).isNotNegative();
		});
	}

	private void checkMemoryMetrics(JVMMemoryMetrics memoryMetrics) {
		assertThat(memoryMetrics).isNotNull();
		checkSingleMemoryMetric(memoryMetrics.heap_memory);
		checkSingleMemoryMetric(memoryMetrics.non_heap_memory);
		assertThat(memoryMetrics.heap_pools_memory).allSatisfy(this::checkSingleMemoryMetric);
	}

	private void checkSingleMemoryMetric(JVMMemoryMetric memoryMetric) {
		assertThat(memoryMetric).isNotNull();
		assertThat(memoryMetric.name).isNotEmpty().isNotBlank();
		assertThat(memoryMetric.used).isNotNegative();
		assertThat(memoryMetric.commited).isNotNegative();
		assertThat(memoryMetric.max).isGreaterThanOrEqualTo(-1);
	}

}
