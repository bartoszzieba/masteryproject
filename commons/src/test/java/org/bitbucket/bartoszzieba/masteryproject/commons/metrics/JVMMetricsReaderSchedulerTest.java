package org.bitbucket.bartoszzieba.masteryproject.commons.metrics;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;
import org.bitbucket.bartoszzieba.masteryproject.commons.logger.JsonSimulationLogger;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.gson.Gson;

public class JVMMetricsReaderSchedulerTest {

	@Test
	public void shouldThrowExceptionWhenInstanceIsStoppedAndStopMetodIsInvoked() {
		JVMMetricsReaderScheduler scheduler = new JVMMetricsReaderScheduler(100, null);
		AbstractThrowableAssert<?, ? extends Throwable> exception = assertThatThrownBy(() -> scheduler.stop());

		exception.isInstanceOf(IllegalStateException.class);
	}

	@Test
	public void shouldThrowExceptionWhenStartMetodIsInvokedTwoTimes() {
		JVMMetricsReaderScheduler scheduler = new JVMMetricsReaderScheduler(Long.MAX_VALUE, null);

		scheduler.start();

		AbstractThrowableAssert<?, ? extends Throwable> exception = assertThatThrownBy(() -> scheduler.start());

		exception.isInstanceOf(IllegalStateException.class);

		scheduler.stop();
	}

	@Test
	public void shouldWriteToLogger() throws InterruptedException {
		JsonSimulationLogger logger = Mockito.mock(JsonSimulationLogger.class);
		long interval = 200;
		JVMMetricsReaderScheduler scheduler = new JVMMetricsReaderScheduler(interval, logger);

		scheduler.start();
		Thread.sleep(2 * interval);
		scheduler.stop();

		Mockito.verify(logger, Mockito.atLeast(1)).writeGsonable(Mockito.any());
	}

	@Test
	public void shouldParseOutputWithSchemaAndCheckIntervals() throws InterruptedException {
		MyOutputStream outputStream = new MyOutputStream();
		JsonSimulationLogger logger = new JsonSimulationLogger(new PrintStream(outputStream));
		long interval = 200;
		JVMMetricsReaderScheduler scheduler = new JVMMetricsReaderScheduler(interval, logger);

		scheduler.start();
		Thread.sleep(2 * interval);
		scheduler.stop();
		Thread.sleep(2 * interval);

		Gson gson = new Gson();

		List<JVMMetrics> metrics = Arrays.stream(new String(outputStream.getOutput()).split("\n"))
				.map(json -> gson.fromJson(json, JVMMetrics.class)).collect(toList());

		// Very ugly assertions
		// timestamp(n+1) - timestamp(n) = interval +/- 10%
		Offset<Long> offset = Offset.offset((long) (interval * 0.1));
		assertThat(metrics.size()).isGreaterThanOrEqualTo(2);
		metrics.stream().map(metric -> metric.timestamp).sorted().reduce((x, y) -> {
			Assertions.assertThat(y - x).isCloseTo(interval, offset);
			return y;
		});

	}

	class MyOutputStream extends OutputStream {

		private final List<Byte> output = new ArrayList<>();

		@Override
		public void write(int b) throws IOException {
			output.add((byte) b);
		}

		public byte[] getOutput() {
			byte[] array = new byte[output.size()];
			for (int i = 0; i < array.length; i++) {
				array[i] = output.get(i);
			}
			return array;
		}

	}

}
