package org.bitbucket.bartoszzieba.masteryproject.commons.api;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class TimeDataTest {

	@Test
	public void shouldBeNanoDurationGraterThanZeroIn10000Tests() {
		for (int i = 0; i < 10000; i++) {
			TimeData timeData = new TimeData();
			timeData.updateEndNanos();
			Assertions.assertThat(timeData.time_end_nanos - timeData.time_start_nanos).isGreaterThan(0);
		}
	}

}
