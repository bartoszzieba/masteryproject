package org.bitbucket.bartoszzieba.masteryproject.commons.logger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.AbstractThrowableAssert;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.TimeData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

public class JsonSimulationLoggerTest {
	private String temporaryFileName;

	@Before
	public void init() throws IOException {
		temporaryFileName = Files.createTempFile(null, null).toString();
	}

	@After
	public void after() {
		new File(temporaryFileName).delete();
	}

	@Test
	public void shouldOpenFileAndLogPropertly() throws IOException {
		List<String> messagesToWrite = Arrays.asList("message 1", "Second message", "Very important msg");
		JsonSimulationLogger logger = new JsonSimulationLogger(temporaryFileName);
		messagesToWrite.forEach(logger::writeLine);
		logger.close();

		List<String> linesReadedFromFile = Files.readAllLines(Paths.get(temporaryFileName));

		assertThat(linesReadedFromFile).isEqualTo(messagesToWrite);
	}

	@Test
	public void shouldThrowExeptionWhenPathToFileNotExist() {
		AbstractThrowableAssert<?, ? extends Throwable> exceptionAssert = assertThatThrownBy(
				() -> new JsonSimulationLogger("/tmp/sxa/fas/thatFileShlodNotExist"));

		exceptionAssert.isInstanceOf(UncheckedIOException.class);
	}

	@Test
	public void shouldWriteGsonableObject() throws Exception {
		JsonSimulationLogger logger = new JsonSimulationLogger(temporaryFileName);
		TimeData timeData = new TimeData();
		timeData.updateEndNanos();

		logger.writeGsonable(timeData);
		logger.close();

		List<String> linesReadedFromFile = Files.readAllLines(Paths.get(temporaryFileName));

		assertThat(linesReadedFromFile).hasSize(1);
		TimeData fromJson = new Gson().fromJson(linesReadedFromFile.get(0), TimeData.class);

		assertThat(fromJson.timestamp).isEqualTo(timeData.timestamp);
		assertThat(fromJson.time_start_nanos).isEqualTo(timeData.time_start_nanos);
		assertThat(fromJson.time_end_nanos).isEqualTo(timeData.time_end_nanos);
	}
}
