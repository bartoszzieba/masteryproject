package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public interface ITestCase {

	public void run(SetBenchmarker<String> setBenchmarker);

}
