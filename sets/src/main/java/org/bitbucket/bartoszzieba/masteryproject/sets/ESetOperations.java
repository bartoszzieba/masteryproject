package org.bitbucket.bartoszzieba.masteryproject.sets;

public enum ESetOperations {
	CONTAINS,
	ADD,
	REMOVE;
}
