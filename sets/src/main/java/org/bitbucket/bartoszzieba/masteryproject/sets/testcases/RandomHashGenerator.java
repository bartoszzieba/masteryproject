package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class RandomHashGenerator {

	private final long startSeed;
	private final ByteBuffer buffer;
	private final MessageDigest sha1;

	private Random rand;

	RandomHashGenerator(long seed) {
		this.startSeed = seed;
		buffer = ByteBuffer.allocate(Long.BYTES);
		restartRandom();
		try {
			sha1 = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	void restartRandom() {
		rand = new Random(startSeed);
	}

	String random() {
		long randomLong = rand.nextLong();
		return randomToHash(randomLong);
	}

	String random(long bind) {
		if (bind < 1) {
			throw new RuntimeException();
		}
		long randomLong = rand.nextLong() % bind;
		randomLong = randomLong < 0 ? randomLong + bind : randomLong;
		return randomToHash(randomLong);
	}

	private String randomToHash(long number) {
		sha1.reset();
		return bytesToHex(sha1.digest(longToBytes(number)));
	}

	private byte[] longToBytes(long x) {
		buffer.clear();
		buffer.putLong(x);
		return buffer.array();
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
