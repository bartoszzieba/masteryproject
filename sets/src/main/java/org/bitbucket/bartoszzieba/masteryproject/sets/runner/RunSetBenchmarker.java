package org.bitbucket.bartoszzieba.masteryproject.sets.runner;

import java.util.Arrays;
import java.util.Set;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.TimestampExtension;
import org.bitbucket.bartoszzieba.masteryproject.commons.logger.JsonSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.commons.metrics.JVMMetricsReaderScheduler;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;
import org.bitbucket.bartoszzieba.masteryproject.sets.testcases.ITestCase;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class RunSetBenchmarker {

	public static void main(String[] args) {
		try {
			RunParams params = new RunParams();
			CmdLineParser parser = new CmdLineParser(params);

			try {
				parser.parseArgument(args);
				if (params.help) {
					params.setProvider.printExtraVars();
					params.testCaseSupplier.printExtraVars();
				} else {
					new RunSetBenchmarker(params).run();
				}
			} catch (CmdLineException e) {
				System.err.println(e.getMessage());
				parser.printUsage(System.err);
				System.exit(1);
			}
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			System.err.println(Arrays.toString(e.getStackTrace()));
			System.err.println(e);
		}
	}

	private final RunParams params;

	public RunSetBenchmarker(RunParams params) {
		this.params = params;
	}

	public void run() throws InterruptedException {
		if (!params.loggerPath.isDirectory()) {
			throw new RuntimeException("logger paths should be a directory");
		}
		System.out.println(params.extraParams);

		JsonSimulationLogger metricsLogger = new JsonSimulationLogger(params.jvmMetricsFile.getPath());

		JVMMetricsReaderScheduler metricsReaderScheduler = new JVMMetricsReaderScheduler(500, metricsLogger);

		Set<String> set = params.setProvider.getInstance(params.extraParams);

		SetBenchmarker<String> benchmarker = new SetBenchmarker<>(set);

		ITestCase testCase = params.testCaseSupplier.getInstance(params.extraParams, params.loggerPath);

		System.gc();
		Thread.sleep(1000);
		TimestampExtension.APPLICATION_START_TIME = System.currentTimeMillis();

		metricsReaderScheduler.start();
		testCase.run(benchmarker);
		metricsReaderScheduler.stop();

		metricsLogger.close();
	}

}
