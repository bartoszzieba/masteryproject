package org.bitbucket.bartoszzieba.masteryproject.sets.setsuppliers;

import java.util.Map;
import java.util.Set;

import net.openhft.chronicle.set.ChronicleSet;
import net.openhft.chronicle.set.ChronicleSetBuilder;

public class ChronicleSetProvider {

	private static final String SET_SIZE_OPTION = "chronicle.size";

	public static final String OPTION_MESSAGE = String.format("To specify a Set size of set use: %s=int",
			SET_SIZE_OPTION);

	public Set<String> createInstance(Map<String, String> vars) {
		int createdSize = vars.containsKey(SET_SIZE_OPTION) ? Integer.parseInt(vars.get(SET_SIZE_OPTION))
				: findMaxSize();
		System.out.println(createdSize);
		return ChronicleSetBuilder.of(String.class).constantKeySizeBySample("0123456789012345678901234567890123456789")
				.entries(createdSize).create();
	}

	private int findMaxSize() {
		int minSize = 0;
		int maxSize = Integer.MAX_VALUE;
		int size = maxSize;
		int createdSize = -1;
		int maxIter = 10;
		int iter = 0;
		while (createdSize == -1 || iter < maxIter) {
			try {
				ChronicleSet<String> create = ChronicleSetBuilder.of(String.class).averageKeySize(40).entries(size)
						.create();
				create.close();
				createdSize = size;
				minSize = size;
			} catch (Throwable e) {
				maxSize = size;
			}
			size = minSize + (maxSize - minSize) / 2;
			iter++;
		}
		return createdSize;
	}

}
