package org.bitbucket.bartoszzieba.masteryproject.sets.setsuppliers;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Function;

import org.apache.commons.collections4.set.MapBackedSet;

public enum EAvailableSets {
	HASH_SET(vars -> new HashSet<>()),
	TREE_SET(vars -> new TreeSet<>()),
	CONCURRENT_SKIP_LIST_SET(vars -> new ConcurrentSkipListSet<>()),
	CONCURRENT_HASH_SET(vars -> MapBackedSet.mapBackedSet(new ConcurrentHashMap<String, Object>(), new Object())),
	CHRONICLE_SET(vars -> new ChronicleSetProvider().createInstance(vars), ChronicleSetProvider.OPTION_MESSAGE);

	private static final String NO_EXTRA_VARS_MSG = "This Set have not extra vars";
	private final Function<Map<String, String>, Set<String>> provider;
	private final String extraVarsMsg;

	private EAvailableSets(Function<Map<String, String>, Set<String>> provider) {
		this(provider, NO_EXTRA_VARS_MSG);
	}

	private EAvailableSets(Function<Map<String, String>, Set<String>> provider, String extraVarsMsg) {
		this.provider = provider;
		this.extraVarsMsg = extraVarsMsg;
	}

	public Set<String> getInstance(Map<String, String> vars) {
		return provider.apply(vars);
	}

	public void printExtraVars() {
		System.out.println(extraVarsMsg);
	}

}
