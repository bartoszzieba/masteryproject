package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class FullFillTestCase implements ITestCase {

	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(SEED);

	private final long seed;

	private final File loggerDirectory;

	public FullFillTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {
		RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
		try (CsvSimulationLogger<SetApi> logger = LoggerHelper.createLogger(loggerDirectory, "FullFill.log")) {
			try {
				while (true) {
					setBenchmarker.add(randomHashGenerator.random(), logger);
				}
			} catch (Error e) {
				setBenchmarker.clear();
				System.out.println("END OF TEST");
			}
		}
	}

}
