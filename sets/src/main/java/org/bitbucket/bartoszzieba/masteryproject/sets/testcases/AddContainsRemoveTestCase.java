package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class AddContainsRemoveTestCase implements ITestCase {

	private static final ExtraVariable MAX_CAPACITY = new ExtraVariable("capacity.max", "long");
	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(MAX_CAPACITY, SEED);

	private final long seed;
	private final long maxCapacity;
	private final File loggerDirectory;

	public AddContainsRemoveTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
		maxCapacity = Long.parseLong(vars.get(MAX_CAPACITY.name));
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {
		RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
		try (CsvSimulationLogger<SetApi> logger = LoggerHelper.createLogger(loggerDirectory, "AddContainsRemove.log")) {

			generateHashes(randomHashGenerator, maxCapacity).forEach(hash -> setBenchmarker.add(hash, logger));
			randomHashGenerator.restartRandom();
			generateHashes(randomHashGenerator, maxCapacity).forEach(hash -> setBenchmarker.contains(hash, logger));
			randomHashGenerator.restartRandom();
			generateHashes(randomHashGenerator, maxCapacity).forEach(hash -> setBenchmarker.remove(hash, logger));
		}
	}

	private Stream<String> generateHashes(RandomHashGenerator randomHashGenerator, long loadLimit) {
		return Stream.generate(randomHashGenerator::random).limit(loadLimit);
	}

}
