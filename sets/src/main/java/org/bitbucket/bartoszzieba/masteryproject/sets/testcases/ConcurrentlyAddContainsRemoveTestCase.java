package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class ConcurrentlyAddContainsRemoveTestCase implements ITestCase {

	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");
	private static final ExtraVariable NUMBER_OF_CORS = new ExtraVariable("thread.limit", "int", "'number of CPU's'");
	private static final ExtraVariable MAX_CAPACITY = new ExtraVariable("capacity.max", "long");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(SEED, NUMBER_OF_CORS, MAX_CAPACITY);

	private final long seed;
	private final long maxCapacity;
	private final int threadsNumber;
	private final File loggerDirectory;

	public ConcurrentlyAddContainsRemoveTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
		maxCapacity = Long.parseLong(vars.get(MAX_CAPACITY.name));
		threadsNumber = vars.containsKey(NUMBER_OF_CORS.name) ? Integer.parseInt(vars.get(NUMBER_OF_CORS.name))
				: Runtime.getRuntime().availableProcessors();
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {
		long operationPerThread = maxCapacity / threadsNumber;
		Random random = new Random(seed);
		List<SeedAndLogger> seedAndLoggers = Stream.generate(random::nextLong).distinct()
				.limit(threadsNumber).map(
						seed -> new SeedAndLogger(seed,
								LoggerHelper.createLogger(loggerDirectory,
										String.format("ConcurrentlyAddContainsRemove-%d.log", seed))))
				.collect(toList());

		createAndExecute(operationPerThread, seedAndLoggers, setBenchmarker::add);
		createAndExecute(operationPerThread, seedAndLoggers, setBenchmarker::contains);
		createAndExecute(operationPerThread, seedAndLoggers, setBenchmarker::remove);

		seedAndLoggers.stream().map(pair -> pair.logger).forEach(CsvSimulationLogger::close);
	}

	private void createAndExecute(long operationPerThread, List<SeedAndLogger> seedAndLoggers,
			BiConsumer<String, CsvSimulationLogger<SetApi>> consumer) {
		List<Executor> executors = seedAndLoggers.stream()
				.map(pair -> new Executor(consumer, operationPerThread, pair.seed, pair.logger)).collect(toList());

		executors.forEach(Thread::start);

		for (Executor executor : executors) {
			try {
				executor.join();
			} catch (InterruptedException ex) {
			}
		}
	}

	private static class SeedAndLogger {

		final long seed;
		final CsvSimulationLogger<SetApi> logger;

		public SeedAndLogger(long seed, CsvSimulationLogger<SetApi> logger) {
			this.seed = seed;
			this.logger = logger;
		}

	}

	static class Executor extends Thread {

		private final long seed;
		private final long operationPerThread;
		private final BiConsumer<String, CsvSimulationLogger<SetApi>> consumer;
		private final CsvSimulationLogger<SetApi> logger;

		public Executor(BiConsumer<String, CsvSimulationLogger<SetApi>> consumer, long maxOperationPerCore, long seed,
				CsvSimulationLogger<SetApi> logger) {
			this.consumer = consumer;
			this.operationPerThread = maxOperationPerCore;
			this.seed = seed;
			this.logger = logger;
		}

		@Override
		public void run() {
			RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
			Stream.generate(randomHashGenerator::random).limit(operationPerThread)
					.forEach(hash -> consumer.accept(hash, logger));
		}
	}

}
