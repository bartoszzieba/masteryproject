package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.io.File;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;

public class LoggerHelper {

	public static CsvSimulationLogger<SetApi> createLogger(File directory, String fileName) {
		String loggerParh = directory.getAbsolutePath() + File.separator + fileName;
		return new CsvSimulationLogger<>(loggerParh);
	}

}
