package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public enum ETestCases {
	FULL_FILL((vars, loggerDirectory) -> new FullFillTestCase(vars, loggerDirectory), FullFillTestCase.EXTRA_VARIABLES),
	ADD_CONTAINS_REMOVE((vars, loggerDirectory) -> new AddContainsRemoveTestCase(vars, loggerDirectory),
			AddContainsRemoveTestCase.EXTRA_VARIABLES),
	FIND_ELEMENTS_WITH_ODD_COUNT_IN_STREAM(
			(vars, loggerDirectory) -> new FindElementsWithOddCountInStreamTestCase(vars, loggerDirectory),
			FindElementsWithOddCountInStreamTestCase.EXTRA_VARIABLES),
	CONCURRENT_FULL_FILL((vars, loggerDirectory) -> new ConcurrentFullFillTestCase(vars, loggerDirectory),
			ConcurrentFullFillTestCase.EXTRA_VARIABLES),
	CONCURRENTLY_ADD_CONTAINS_REMOVE(
			(vars, loggerDirectory) -> new ConcurrentlyAddContainsRemoveTestCase(vars, loggerDirectory),
			ConcurrentlyAddContainsRemoveTestCase.EXTRA_VARIABLES),
	CONCURRENTLY_FIND_ELEMENTS_WITH_ODD_COUNT_IN_STREAM(
			(vars, loggerDirectory) -> new ConcurrentlyFindElementsWithOddCountInStreamTestCase(vars, loggerDirectory),
			ConcurrentlyFindElementsWithOddCountInStreamTestCase.EXTRA_VARIABLES);

	private final BiFunction<Map<String, String>, File, ITestCase> provider;
	private final List<ExtraVariable> extraVariables;

	private ETestCases(BiFunction<Map<String, String>, File, ITestCase> provider, List<ExtraVariable> extraVariables) {
		this.provider = provider;
		this.extraVariables = extraVariables;
	}

	public ITestCase getInstance(Map<String, String> vars, File loggerPath) {
		return provider.apply(vars, loggerPath);
	}

	public void printExtraVars() {
		extraVariables.stream().map(ExtraVariable::toString).forEach(System.out::println);
	}

}
