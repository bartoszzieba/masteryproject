package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class ConcurrentlyFindElementsWithOddCountInStreamTestCase implements ITestCase {

	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");
	private static final ExtraVariable BIND = new ExtraVariable("random.bind", "long");
	private static final ExtraVariable STREAM_LENGTH = new ExtraVariable("stream.length", "long");
	private static final ExtraVariable NUMBER_OF_THREADS = new ExtraVariable("thread.limit", "int",
			"'number of CPU's'");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(SEED, BIND, STREAM_LENGTH,
			NUMBER_OF_THREADS);

	private final long seed;
	private final long bind;
	private final long streamLength;
	private final File loggerDirectory;
	private final int threadsLimit;

	public ConcurrentlyFindElementsWithOddCountInStreamTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
		bind = Long.parseLong(vars.get(BIND.name));
		streamLength = Long.parseLong(vars.get(STREAM_LENGTH.name));
		threadsLimit = vars.containsKey(NUMBER_OF_THREADS.name) ? Integer.parseInt(vars.get(NUMBER_OF_THREADS.name))
				: Runtime.getRuntime().availableProcessors();
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {
		long streamLengthPerThread = streamLength / threadsLimit;
		Random random = new Random(seed);
		List<Executor> executors = Stream.generate(random::nextLong).distinct().limit(threadsLimit)
				.map(seed -> new Executor(setBenchmarker, seed, streamLengthPerThread,
						LoggerHelper.createLogger(loggerDirectory, String.format("ConcurrentFullFill-%d.log", seed))))
				.collect(toList());
		executors.forEach(Thread::start);

		for (Executor executor : executors) {
			try {
				executor.join();
			} catch (InterruptedException e) {
			}
		}
	}

	private class Executor extends Thread {

		private final SetBenchmarker<String> setBenchmarker;
		private final long seed;
		private final CsvSimulationLogger<SetApi> logger;
		private final long streamLengthPerThread;

		public Executor(SetBenchmarker<String> setBenchmarker, long seed, long streamLengthPerThread,
				CsvSimulationLogger<SetApi> logger) {
			this.setBenchmarker = setBenchmarker;
			this.seed = seed;
			this.streamLengthPerThread = streamLengthPerThread;
			this.logger = logger;
		}

		@Override
		public void run() {
			RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
			Stream.generate(() -> randomHashGenerator.random(bind)).limit(streamLengthPerThread)
					.forEach(element -> checkAndChange(element, setBenchmarker));
		}

		private void checkAndChange(String element, SetBenchmarker<String> setBenchmarker) {
			if (setBenchmarker.contains(element, logger)) {
				setBenchmarker.remove(element, logger);
			} else {
				setBenchmarker.add(element, logger);
			}
		}
	}

}
