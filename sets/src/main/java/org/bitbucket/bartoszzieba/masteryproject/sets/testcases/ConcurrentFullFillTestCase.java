package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class ConcurrentFullFillTestCase implements ITestCase {

	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");
	private static final ExtraVariable NUMBER_OF_CORS = new ExtraVariable("thread.limit", "int", "'number of CPU's'");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(SEED, NUMBER_OF_CORS);

	private final long seed;
	private final int threadsLimit;
	private final File loggerDirectory;

	public ConcurrentFullFillTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
		threadsLimit = vars.containsKey(NUMBER_OF_CORS.name) ? Integer.parseInt(vars.get(NUMBER_OF_CORS.name))
				: Runtime.getRuntime().availableProcessors();
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {

		Random random = new Random(seed);
		List<Executor> executors = Stream.generate(random::nextLong).distinct().limit(threadsLimit)
				.map(seed -> new Executor(setBenchmarker, seed,
						LoggerHelper.createLogger(loggerDirectory, String.format("ConcurrentFullFill-%d.log", seed))))
				.collect(toList());

		executors.forEach(e -> e.setNeighborsThreads(executors));
		executors.forEach(Executor::start);

		for (Executor executor : executors) {
			try {
				executor.join();
			} catch (InterruptedException e) {
			}
		}

	}

	static class Executor extends Thread {

		private final SetBenchmarker<String> setBenchmarker;
		private final long seed;

		private boolean isEnd = false;
		private final CsvSimulationLogger<SetApi> logger;
		private List<Executor> executors;

		public Executor(SetBenchmarker<String> setBenchmarker, long seed,
				CsvSimulationLogger<SetApi> csvSimulationLogger) {
			this.setBenchmarker = setBenchmarker;
			this.seed = seed;
			this.logger = csvSimulationLogger;
		}

		public void setNeighborsThreads(List<Executor> executors) {
			this.executors = executors;
		}

		public void notifyEnd() {
			isEnd = true;
		}

		@Override
		public void run() {
			RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
			try {
				while (!isEnd) {
					setBenchmarker.add(randomHashGenerator.random(), logger);
				}
			} catch (Throwable e) {
			}
			executors.forEach(Executor::notifyEnd);
			logger.close();
		}
	}

}
