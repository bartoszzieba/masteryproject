package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetApi;
import org.bitbucket.bartoszzieba.masteryproject.sets.SetBenchmarker;

public class FindElementsWithOddCountInStreamTestCase implements ITestCase {

	private static final ExtraVariable SEED = new ExtraVariable("random.seed", "long", "1");
	private static final ExtraVariable BIND = new ExtraVariable("random.bind", "long");
	private static final ExtraVariable STREAM_LENGTH = new ExtraVariable("stream.length", "long");

	public static final List<ExtraVariable> EXTRA_VARIABLES = Arrays.asList(SEED, BIND, STREAM_LENGTH);

	private final long seed;
	private final long bind;
	private final long streamLength;
	private final File loggerDirectory;

	public FindElementsWithOddCountInStreamTestCase(Map<String, String> vars, File loggerDirectory) {
		this.loggerDirectory = loggerDirectory;
		seed = vars.containsKey(SEED.name) ? Long.parseLong(vars.get(SEED.name)) : Long.parseLong(SEED.defaultValue);
		bind = Long.parseLong(vars.get(BIND.name));
		streamLength = Long.parseLong(vars.get(STREAM_LENGTH.name));
	}

	@Override
	public void run(SetBenchmarker<String> setBenchmarker) {
		RandomHashGenerator randomHashGenerator = new RandomHashGenerator(seed);
		try (CsvSimulationLogger<SetApi> logger = LoggerHelper.createLogger(loggerDirectory,
				"FindElementsWithOddCountInStream.log")) {

			Stream.generate(() -> randomHashGenerator.random(bind)).limit(streamLength)
					.forEach(element -> checkAndChange(element, setBenchmarker, logger));

		}
	}

	private void checkAndChange(String element, SetBenchmarker<String> setBenchmarker,
			CsvSimulationLogger<SetApi> logger) {
		if (setBenchmarker.contains(element, logger)) {
			setBenchmarker.remove(element, logger);
		} else {
			setBenchmarker.add(element, logger);
		}
	}

}
