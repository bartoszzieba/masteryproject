package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

public class ExtraVariable {

	public final String name;
	public final String type;
	public final String defaultValue;
	public final boolean required;

	public ExtraVariable(String name, String type, String defaultValue) {
		this(name, type, defaultValue, false);
	}

	public ExtraVariable(String name, String type) {
		this(name, type, "", true);
	}

	private ExtraVariable(String name, String type, String defaultValue, boolean required) {
		this.name = name;
		this.type = type;
		this.defaultValue = defaultValue;
		this.required = required;
	}

	@Override
	public String toString() {
		return String.format("name: %s, type: %s, defaultValue: %s, required: %b", name, type, defaultValue, required);
	}

}
