package org.bitbucket.bartoszzieba.masteryproject.sets;

import static org.bitbucket.bartoszzieba.masteryproject.sets.ESetOperations.ADD;
import static org.bitbucket.bartoszzieba.masteryproject.sets.ESetOperations.CONTAINS;
import static org.bitbucket.bartoszzieba.masteryproject.sets.ESetOperations.REMOVE;

import java.util.Set;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;

public class SetBenchmarker<E> {

	private final Set<E> set;

	public SetBenchmarker(Set<E> set) {
		this.set = set;
	}

	public int size() {
		return set.size();
	}

	public boolean isEmpty() {
		return set.isEmpty();
	}

	public boolean contains(Object o, CsvSimulationLogger<SetApi> logger) {
		SetApi setApi = new SetApi(CONTAINS, set.size());
		boolean ret = set.contains(o);
		updateAndWrite(setApi, logger);
		return ret;
	}

	private void updateAndWrite(SetApi setApi, CsvSimulationLogger<SetApi> logger) {
		setApi.updateEndNanos();
		logger.writeElement(setApi);
	}

	public boolean add(E e, CsvSimulationLogger<SetApi> logger) {
		SetApi setApi = new SetApi(ADD, set.size());
		boolean ret = set.add(e);
		updateAndWrite(setApi, logger);
		return ret;

	}

	public boolean remove(Object o, CsvSimulationLogger<SetApi> logger) {
		SetApi setApi = new SetApi(REMOVE, set.size());
		boolean ret = set.remove(o);
		updateAndWrite(setApi, logger);
		return ret;
	}

	public void clear() {
		set.clear();
	}

}
