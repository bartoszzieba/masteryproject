package org.bitbucket.bartoszzieba.masteryproject.sets;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.Csvable;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.TimeData;

public class SetApi extends TimeData implements Csvable {

	private static final String SCHEMA = "%s,%d,%d,%d,%d";

	public ESetOperations operation;
	public int set_size;

	public SetApi(ESetOperations operation, int set_size) {
		this.operation = operation;
		this.set_size = set_size;
	}

	public SetApi() {
		// For Gson
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}

	@Override
	public String toCsvLine() {
		return String.format(SCHEMA, operation.toString(), set_size, time_start_nanos, time_end_nanos, timestamp);
	}

}
