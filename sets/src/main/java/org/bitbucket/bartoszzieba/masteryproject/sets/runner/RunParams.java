package org.bitbucket.bartoszzieba.masteryproject.sets.runner;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bitbucket.bartoszzieba.masteryproject.sets.setsuppliers.EAvailableSets;
import org.bitbucket.bartoszzieba.masteryproject.sets.testcases.ETestCases;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.MapOptionHandler;

public class RunParams {

	@Option(name = "-e", depends = { "-s", "-t" }, usage = "print extra vars to test case and set creation")
	public boolean help;

	@Option(name = "-o", depends = { "-m", "-s", "-t" }, usage = "Logger output directory")
	public File loggerPath;

	@Option(name = "-m", depends = { "-o", "-s", "-t" }, usage = "Jvm Stats output file")
	public File jvmMetricsFile;

	@Option(name = "-s", required = true, usage = "Set type name")
	public EAvailableSets setProvider;

	@Option(name = "-t", required = true, usage = "Test case")
	public ETestCases testCaseSupplier;

	@Option(name = "-p", required = false, usage = "Extra params to test case or instance creation", handler = MapOptionHandler.class)
	public Map<String, String> extraParams = new HashMap<>();

}
