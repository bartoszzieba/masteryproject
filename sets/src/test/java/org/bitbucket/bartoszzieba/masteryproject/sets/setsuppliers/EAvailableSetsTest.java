package org.bitbucket.bartoszzieba.masteryproject.sets.setsuppliers;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class EAvailableSetsTest {

	@Test
	public void shouldGetAllSet() {
		for (EAvailableSets setSupplier : EAvailableSets.values()) {
			Set<String> stringSet = setSupplier.getInstance(getConfig());
			assertThat(stringSet.isEmpty()).isTrue();
		}
	}

	private Map<String, String> getConfig() {
		HashMap<String, String> config = new HashMap<>();
		config.put("chronicle.size", "100");
		return config;
	}

}
