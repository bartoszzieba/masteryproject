package org.bitbucket.bartoszzieba.masteryproject.sets.testcases;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class RandomHashGeneratorTest {

	private final long seed = 1;
	private RandomHashGenerator randomHashGenerator;

	@Before
	public void setup() {
		randomHashGenerator = new RandomHashGenerator(seed);
	}

	@Test
	public void shouldReturnSha1Hex() {
		String random = randomHashGenerator.random();

		assertThat(random).hasSize(40);
		assertThat(random).matches(Pattern.compile("[A-F0-9]+"));
	}

	@Test
	public void shouldReturnSha1HexWhenIsBind() {
		String random = randomHashGenerator.random(2);

		assertThat(random).hasSize(40);
		assertThat(random).matches(Pattern.compile("[A-F0-9]+"));
	}

	@Test
	public void shouldReturnTheSameSequenceAfterRestart() {
		int limit = 100;
		List<String> first = Stream.generate(randomHashGenerator::random).limit(limit).collect(toList());
		randomHashGenerator.restartRandom();
		List<String> second = Stream.generate(randomHashGenerator::random).limit(limit).collect(toList());

		assertThat(first).isEqualTo(second);
	}

	@Test
	public void shouldThrowExceptionWhenBindIsLowerThan1() {
		AbstractThrowableAssert<?, ? extends Throwable> exception = Assertions
				.assertThatThrownBy(() -> randomHashGenerator.random(0));

		exception.isInstanceOf(RuntimeException.class);
	}
}
