package org.bitbucket.bartoszzieba.masteryproject.sets;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.logger.CsvSimulationLogger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SetBenchmarkerTest {

	@Mock
	private CsvSimulationLogger<SetApi> logger;

	private Set<Object> testSet;
	private SetBenchmarker<Object> setBenchmarker;

	@Before
	public void setup() throws IOException {
		testSet = new HashSet<>();
		setBenchmarker = new SetBenchmarker<>(testSet);
	}

	@Test
	public void shouldRunAddOperations() throws IOException {
		int numberOfObjects = 1000;
		List<Object> objects = createObjects(numberOfObjects);
		for (Object o : objects) {
			setBenchmarker.add(o, logger);
		}

		assertThat(testSet).containsAll(objects);
		assertThat(setBenchmarker.isEmpty()).isEqualTo(testSet.isEmpty());
		assertThat(setBenchmarker.size()).isEqualTo(testSet.size());
		Mockito.verify(logger, Mockito.times(numberOfObjects)).writeElement(Mockito.any());
	}

	private List<Object> createObjects(int number) {
		return Stream.generate(() -> new Object()).limit(number).collect(toList());
	}

	@Test
	public void shouldRunContainsOperations() throws IOException {
		int numberOfObjects = 1000;
		List<Object> objects = createObjects(numberOfObjects);
		for (Object o : objects) {
			setBenchmarker.add(o, logger);
		}

		HashSet<Object> randomSequenceOfObjects = new HashSet<>(objects);

		for (Object o : randomSequenceOfObjects) {
			assertThat(setBenchmarker.contains(o, logger)).isTrue();
		}

		List<Object> objectNotInSet = createObjects(numberOfObjects);
		for (Object o : new HashSet<>(objectNotInSet)) {
			assertThat(setBenchmarker.contains(o, logger)).isFalse();
		}

		assertThat(testSet).containsAll(objects);
		assertThat(setBenchmarker.isEmpty()).isEqualTo(testSet.isEmpty());
		assertThat(setBenchmarker.size()).isEqualTo(testSet.size());
		Mockito.verify(logger, Mockito.times(3 * numberOfObjects)).writeElement(Mockito.any());
	}

	@Test
	public void shouldRunRemoveOperations() throws IOException {
		int numberOfObjects = 1000;
		List<Object> objects = createObjects(numberOfObjects);
		for (Object o : objects) {
			setBenchmarker.add(o, logger);
		}

		for (Object o : objects) {
			assertThat(setBenchmarker.remove(o, logger));
		}

		assertThat(testSet).isEmpty();
		assertThat(setBenchmarker.isEmpty()).isEqualTo(testSet.isEmpty());
		assertThat(setBenchmarker.size()).isEqualTo(testSet.size());
		Mockito.verify(logger, Mockito.times(2 * numberOfObjects)).writeElement(Mockito.any());
	}

	@Test
	public void shouldMethodCleanWork() {
		int numberOfObjects = 1000;
		List<Object> objects = createObjects(numberOfObjects);
		for (Object o : objects) {
			setBenchmarker.add(o, logger);
		}

		assertThat(setBenchmarker.isEmpty()).isFalse();
		setBenchmarker.clear();
		assertThat(setBenchmarker.isEmpty()).isTrue();
		assertThat(testSet.isEmpty()).isTrue();
	}

}
