package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CsvByTimestampCounterAgregatorTest {

	CsvByTimestampCounterAgregator agregator = new CsvByTimestampCounterAgregator();

	private Path temporaryFile;

	@Before
	public void init() throws IOException {
		temporaryFile = Files.createTempFile(null, null);
	}

	@After
	public void after() {
		temporaryFile.toFile().delete();
	}

	@Test
	public void shouldAgregateData() throws IOException {
		PrintStream out = new PrintStream(temporaryFile.toFile());
		agregator.setParams(prepareParams());
		agregator.setOutputStream(out);
		for (String line : prepareData()) {
			agregator.inputLine(line);
		}
		agregator.finish();
		out.flush();
		out.close();
		List<String> readAllLines = Files.readAllLines(temporaryFile);
		assertThat(readAllLines).hasSize(5);
		assertThat(readAllLines).containsSequence("timestamp,count", "5,5", "15,2", "25,0", "35,1");
	}

	private Map<String, String> prepareParams() {
		HashMap<String, String> map = new HashMap<>();
		map.put("timestamp.columnIndex", "0");
		map.put("timestamp.period", "10");
		return map;
	}

	private List<String> prepareData() {
		List<String> list = new ArrayList<>();
		list.add("1,19");
		list.add("1,19");
		list.add("1,19");
		list.add("3,19");
		list.add("10,31");
		list.add("11,31");
		list.add("14,31");
		list.add("31,31");
		return list;
	}

}
