package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CsvTimeExecutionBySizeAgregatorTest {

	private final CsvTimeExecutionBySizeAgregator agregator = new CsvTimeExecutionBySizeAgregator();

	private Path temporaryFile;

	private PrintStream out;

	@Before
	public void init() throws IOException {
		temporaryFile = Files.createTempFile(null, null);
		out = new PrintStream(temporaryFile.toFile());
	}

	@After
	public void after() {
		temporaryFile.toFile().delete();
	}

	@Test
	public void shouldAgregateData() throws IOException, URISyntaxException {
		agregator.setParams(prepareConfig());
		agregator.setOutputStream(out);
		for (String line : prepareData()) {
			agregator.inputLine(line);
		}
		agregator.finish();
		out.close();

		List<String> lines = Files.readAllLines(temporaryFile);
		assertThat(lines).hasSize(11);
		assertThat(lines.get(0)).isEqualTo("size,avg_duration");
		assertThat(lines.get(1)).isEqualTo("5,247769");
	}

	private List<String> prepareData() throws IOException, URISyntaxException {
		return Files.readAllLines(Paths.get(getClass().getResource("CsvTimeExecutionBySizeAgregatorTest.csv").toURI()));
	}

	private Map<String, String> prepareConfig() {
		HashMap<String, String> config = new HashMap<>();
		config.put("size.range", "10");
		return config;
	}
}
