package org.bitbucket.bartoszzieba.masteryproject.agregator.processor;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bitbucket.bartoszzieba.masteryproject.agregator.agregators.CsvByTimestampCounterAgregator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LineDataProcessorTest {
	CsvByTimestampCounterAgregator agregator = new CsvByTimestampCounterAgregator();

	private Path temporaryOutputPath;

	@Before
	public void init() throws IOException {
		agregator.setParams(prepareParams());
		temporaryOutputPath = Files.createTempFile(null, null);
	}

	private Map<String, String> prepareParams() {
		HashMap<String, String> map = new HashMap<>();
		map.put("timestamp.columnIndex", "0");
		map.put("timestamp.period", "10");
		return map;
	}

	@After
	public void after() {
		temporaryOutputPath.toFile().delete();
	}

	@Test
	public void shouldProccessData() throws IOException {
		Path inputPath = Paths.get(getClass().getResource("inputFile.csv").getPath());
		LineDataProcessor lineDataProcessor = new LineDataProcessor(inputPath, temporaryOutputPath, agregator);
		lineDataProcessor.process();

		List<String> readAllLines = Files.readAllLines(temporaryOutputPath);
		assertThat(readAllLines).hasSize(5);
		assertThat(readAllLines).containsSequence("timestamp,count", "5,5", "15,2", "25,0", "35,1");
	}

}
