package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JvmGCMetricsAgregatorTest {

	JvmGCMetricsAgregator agregator = new JvmGCMetricsAgregator();

	private Path temporaryFile;

	private PrintStream out;

	@Before
	public void init() throws IOException {
		temporaryFile = Files.createTempFile(null, null);
		out = new PrintStream(temporaryFile.toFile());
	}

	@After
	public void after() {
		temporaryFile.toFile().delete();
	}

	@Test
	public void shouldParseInput() throws IOException, URISyntaxException {
		agregator.setParams(new HashMap<>());
		agregator.setOutputStream(out);
		for (String line : preadData()) {
			agregator.inputLine(line);
		}
		agregator.finish();
		out.close();

		List<String> lines = Files.readAllLines(temporaryFile);
		assertThat(lines).hasSize(3);
		assertThat(lines.get(0)).isEqualTo("timestamp,PS Scavenge,PS MarkSweep");
		assertThat(lines.get(1)).isEqualTo("11,2,10");
		assertThat(lines.get(2)).isEqualTo("535,21,10");
	}

	private List<String> preadData() throws IOException, URISyntaxException {
		return Files.readAllLines(Paths.get(getClass().getResource("jvm.log").toURI()));
	}

}
