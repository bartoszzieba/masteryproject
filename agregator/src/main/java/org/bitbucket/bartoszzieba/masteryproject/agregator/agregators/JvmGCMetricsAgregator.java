package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMGarbageCollectorMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;

import com.google.gson.Gson;

public class JvmGCMetricsAgregator implements IAgregator {

	private final Gson gson = new Gson();
	private PrintStream out;
	private boolean isFirstLine;
	private String schema;
	private int gcsCount;
	private Map<String, Integer> gcToColumn;
	private int schemaElementsCount;

	@Override
	public void setParams(Map<String, String> params) {
		// nothing to do
	}

	@Override
	public void setOutputStream(PrintStream out) {
		this.out = out;
		isFirstLine = true;
		gcToColumn = new HashMap<>();
	}

	@Override
	public void inputLine(String string) {
		JVMMetrics jvmMetrics = gson.fromJson(string, JVMMetrics.class);
		List<JVMGarbageCollectorMetric> gcMetrics = jvmMetrics.garbage_collestors_metric;
		if (isFirstLine) {
			parseSchema(gcMetrics);
		}
		if (gcMetrics.size() != gcsCount) {
			throw new RuntimeException();
		}
		Object[] data = new String[schemaElementsCount];
		data[0] = Long.toString(jvmMetrics.timestamp);
		for (JVMGarbageCollectorMetric gcMetric : gcMetrics) {
			data[gcToColumn.get(gcMetric.name)] = Long.toString(gcMetric.collection_time);
		}
		out.printf(schema, data);
	}

	private void parseSchema(List<JVMGarbageCollectorMetric> gcMetrics) {
		gcsCount = gcMetrics.size();
		schemaElementsCount = gcsCount + 1;

		AtomicInteger counter = new AtomicInteger(1);
		gcMetrics.forEach(gcMetric -> gcToColumn.put(gcMetric.name, counter.getAndIncrement()));

		schema = Stream.generate(() -> "%s").limit(schemaElementsCount).collect(Collectors.joining(",")) + "%n";
		Object[] firstLine = new String[schemaElementsCount];
		firstLine[0] = "timestamp";

		gcToColumn.forEach((key, value) -> firstLine[value] = key);

		out.printf(schema, firstLine);

		isFirstLine = false;
	}

	@Override
	public void finish() {
		// nothing to do
	}

}
