package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.io.PrintStream;
import java.util.Map;
import java.util.Optional;

public class CsvByTimestampCounterAgregator implements IAgregator {

	private static final String TIMESTAMP_COLUMN_NAME = "timestamp.columnIndex";
	private static final String TIMESTAMP_PERIOD = "timestamp.period";

	private PrintStream out;
	private int timestampColumnIndex;
	private int timestampPeriod;
	private long counter = 0;
	private long minTimestamp;
	private long maxTimestamp;

	@Override
	public void setParams(Map<String, String> params) {
		timestampColumnIndex = Optional.ofNullable(params.get(TIMESTAMP_COLUMN_NAME)).map(Integer::parseInt).orElse(4);
		timestampPeriod = Optional.ofNullable(params.get(TIMESTAMP_PERIOD)).map(Integer::parseInt).orElse(1000);
		minTimestamp = 0;
		maxTimestamp = timestampPeriod;
	}

	@Override
	public void setOutputStream(PrintStream out) {
		this.out = out;
		out.println("timestamp,count");
	}

	@Override
	public void inputLine(String string) {
		String[] columns = string.split(",");
		long actualTimestamp = Long.parseLong(columns[timestampColumnIndex]);
		if (actualTimestamp <= maxTimestamp) {
			counter++;
		} else {
			do {
				printResult();
				counter = 0;
				minTimestamp += timestampPeriod;
				maxTimestamp += timestampPeriod;
			} while (actualTimestamp > maxTimestamp);
			counter++;
		}
	}

	private void printResult() {
		out.printf("%d,%d\n", minTimestamp + (maxTimestamp - minTimestamp) / 2, counter);
	}

	@Override
	public void finish() {
		printResult();
	}

}
