package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.io.PrintStream;
import java.util.Map;

import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetric;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMemoryMetrics;
import org.bitbucket.bartoszzieba.masteryproject.commons.api.jvm.JVMMetrics;

import com.google.gson.Gson;

public class JvmMemoryAgregator implements IAgregator {

	private final Gson gson = new Gson();
	private PrintStream out;

	@Override
	public void setParams(Map<String, String> params) {
		// nothing to do
	}

	@Override
	public void setOutputStream(PrintStream out) {
		this.out = out;
		out.println("timestamp,heap_used,heap_commited,off_heap_used,off_heap_commited");
	}

	@Override
	public void inputLine(String string) {
		JVMMetrics jvmMetrics = gson.fromJson(string, JVMMetrics.class);
		JVMMemoryMetrics memoryMetrics = jvmMetrics.memory_metrics;
		JVMMemoryMetric heapMemory = memoryMetrics.heap_memory;
		JVMMemoryMetric nonHeapMemory = memoryMetrics.non_heap_memory;
		out.printf("%d,%d,%d,%d,%d%n", jvmMetrics.timestamp, heapMemory.used, heapMemory.commited, nonHeapMemory.used,
				nonHeapMemory.commited);
	}

	@Override
	public void finish() {
		// nothing to do
	}

}
