package org.bitbucket.bartoszzieba.masteryproject.agregator.runner;

import java.io.IOException;

import org.bitbucket.bartoszzieba.masteryproject.agregator.agregators.IAgregator;
import org.bitbucket.bartoszzieba.masteryproject.agregator.processor.LineDataProcessor;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class RunAgregator {

	public static void main(String[] args) throws IOException {
		RunParams params = new RunParams();
		CmdLineParser parser = new CmdLineParser(params);
		try {
			parser.parseArgument(args);
			new RunAgregator().run(params);

		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			parser.printUsage(System.err);
			System.exit(1);
		}
	}

	public void run(RunParams params) throws IOException {
		IAgregator agregator = params.agregator.getInstance();
		agregator.setParams(params.params);
		LineDataProcessor dataProcessor = new LineDataProcessor(params.inputFile, params.outputFile, agregator);
		dataProcessor.process();
	}

}
