package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.io.PrintStream;
import java.util.Map;

public interface IAgregator {

	public void setParams(Map<String, String> params);

	public void setOutputStream(PrintStream out);

	public void inputLine(String string);

	public void finish();

}
