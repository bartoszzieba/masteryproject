package org.bitbucket.bartoszzieba.masteryproject.agregator.processor;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.bitbucket.bartoszzieba.masteryproject.agregator.agregators.IAgregator;

public class LineDataProcessor {

	private final Path inputFile;
	private final Path outputFile;
	private final IAgregator agregator;

	public LineDataProcessor(Path inputFile, Path outputFile, IAgregator agregator) {
		this.inputFile = inputFile;
		this.outputFile = outputFile;
		this.agregator = agregator;

	}

	public void process() throws IOException {
		try (Stream<String> input = Files.lines(inputFile); PrintStream out = new PrintStream(outputFile.toFile())) {
			agregator.setOutputStream(out);
			input.forEach(agregator::inputLine);
			agregator.finish();
		}
	}

}
