package org.bitbucket.bartoszzieba.masteryproject.agregator.runner;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.bitbucket.bartoszzieba.masteryproject.agregator.agregators.EAgregators;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.MapOptionHandler;

public class RunParams {
	@Option(name = "-i", required = true, usage = "input file")
	Path inputFile;

	@Option(name = "-o", required = true, usage = "output file")
	Path outputFile;

	@Option(name = "-a", required = true, usage = "agregator type")
	EAgregators agregator;

	@Option(name = "-p", required = false, handler = MapOptionHandler.class, usage = "params for Agregator")
	Map<String, String> params = new HashMap<>();
}
