package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.util.function.Supplier;

public enum EAgregators {
	CSV_BY_TIMESTAMP_COUNTER(CsvByTimestampCounterAgregator::new),
	CSV_TIME_EXECUTION_BY_SIZE(CsvTimeExecutionBySizeAgregator::new),
	JVM_MEMORY(JvmMemoryAgregator::new),
	JVM_GC(JvmGCMetricsAgregator::new);

	private final Supplier<IAgregator> provider;

	private EAgregators(Supplier<IAgregator> provider) {
		this.provider = provider;
	}

	public IAgregator getInstance() {
		return provider.get();
	}

}
