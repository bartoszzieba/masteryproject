package org.bitbucket.bartoszzieba.masteryproject.agregator.agregators;

import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Map;
import java.util.Optional;

public class CsvTimeExecutionBySizeAgregator implements IAgregator {

	private static final String SIZE_COLUMN = "size.columnIndex";
	private static final String SIZE_RANGE = "size.range";
	private static final String START_TIME_COLUMN = "time.start.columnIndex";
	private static final String END_TIME_COLUMN = "time.end.columnIndex";

	private PrintStream out;
	private int sizeColumn;
	private int sizeRange;
	private int startTimeColumn;
	private int endTimeColumn;
	private long endRange;

	private BigInteger counter = BigInteger.ZERO;
	private BigInteger sum = BigInteger.ZERO;

	@Override
	public void setParams(Map<String, String> params) {
		sizeColumn = Optional.ofNullable(params.get(SIZE_COLUMN)).map(Integer::parseInt).orElse(1);
		sizeRange = Optional.ofNullable(params.get(SIZE_RANGE)).map(Integer::parseInt).orElse(1000);
		startTimeColumn = Optional.ofNullable(params.get(START_TIME_COLUMN)).map(Integer::parseInt).orElse(2);
		endTimeColumn = Optional.ofNullable(params.get(END_TIME_COLUMN)).map(Integer::parseInt).orElse(3);
		endRange = sizeRange;
	}

	@Override
	public void setOutputStream(PrintStream out) {
		this.out = out;
		out.println("size,avg_duration");
	}

	@Override
	public void inputLine(String string) {
		String[] columns = string.split(",");
		long size = parseLong(columns[sizeColumn]);
		long startTime = parseLong(columns[startTimeColumn]);
		long endTime = parseLong(columns[endTimeColumn]);
		BigInteger duration = BigInteger.valueOf(endTime - startTime);

		if (size > endRange) {
			printAndRestartState();
			endRange += sizeRange;
		}

		sum = sum.add(duration);
		counter = counter.add(BigInteger.ONE);
	}

	private long parseLong(String number) {
		return Long.parseLong(number);
	}

	private void printAndRestartState() {
		out.printf("%d,%d\n", endRange - sizeRange / 2, sum.divide(counter).longValueExact());
		sum = BigInteger.ZERO;
		counter = BigInteger.ZERO;
	}

	@Override
	public void finish() {
		printAndRestartState();
	}

}
