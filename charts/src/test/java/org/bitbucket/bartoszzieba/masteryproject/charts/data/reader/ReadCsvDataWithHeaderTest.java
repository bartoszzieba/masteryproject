package org.bitbucket.bartoszzieba.masteryproject.charts.data.reader;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;

public class ReadCsvDataWithHeaderTest {

	@Test
	public void shouldReadData() throws Exception {
		Path inputFile = Paths.get(getClass().getResource("inputFile.csv").toURI());

		try (ReadCsvDataWithHeader reader = new ReadCsvDataWithHeader(inputFile)) {
			assertThat(reader.getKeyName()).isEqualTo("timestamp");
			assertThat(reader.getSeriesNames()).containsOnly("PS Scavenge", "PS MarkSweep");

			Optional<Map<String, Long>> firstRow = reader.getNextRow();
			assertThat(firstRow.isPresent()).isTrue();
			assertThat(firstRow.get()).containsAllEntriesOf(expectedFirstRow());

			Optional<Map<String, Long>> secondRow = reader.getNextRow();
			assertThat(secondRow.isPresent()).isTrue();
			assertThat(secondRow.get()).containsAllEntriesOf(expectedSecondRow());

			Optional<Map<String, Long>> unexistingRow = reader.getNextRow();
			assertThat(unexistingRow.isPresent()).isFalse();
		}
	}

	private Map<String, Long> expectedFirstRow() {
		HashMap<String, Long> map = new HashMap<>();
		map.put("timestamp", 11L);
		map.put("PS Scavenge", 2L);
		map.put("PS MarkSweep", 10L);
		return map;
	}

	private Map<String, Long> expectedSecondRow() {
		HashMap<String, Long> map = new HashMap<>();
		map.put("timestamp", 535L);
		map.put("PS Scavenge", 21L);
		map.put("PS MarkSweep", 10L);
		return map;
	}

}
