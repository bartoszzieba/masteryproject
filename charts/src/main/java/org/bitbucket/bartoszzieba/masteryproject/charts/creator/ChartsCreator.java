package org.bitbucket.bartoszzieba.masteryproject.charts.creator;

import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import org.bitbucket.bartoszzieba.masteryproject.charts.data.reader.ReadCsvDataWithHeader;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ChartsCreator {

	public JFreeChart createChart(ReadCsvDataWithHeader reader, EUnitConverter keyConverter,
			EUnitConverter valueConverter, String title, String xAxisLabel, String yAxisLabel,
			boolean switchToAreaChart) throws IOException {
		String keyName = reader.getKeyName();
		Map<String, XYSeries> dataSeries = reader.getSeriesNames().stream()
				.collect(toMap(Function.identity(), XYSeries::new));

		Function<Number, Double> keyConverterFunction = keyConverter.getConverter();
		Function<Number, Double> valueConverterFunction = valueConverter.getConverter();

		Optional<Map<String, Long>> nextRow = reader.getNextRow();
		while (nextRow.isPresent()) {
			Map<String, Long> row = nextRow.get();
			Double keyValue = keyConverterFunction.apply(row.get(keyName));

			for (Entry<String, XYSeries> dataSerieEntry : dataSeries.entrySet()) {
				dataSerieEntry.getValue().add(keyValue, valueConverterFunction.apply(row.get(dataSerieEntry.getKey())));
			}

			nextRow = reader.getNextRow();
		}
		XYSeriesCollection dataSet = new XYSeriesCollection();
		dataSeries.values().forEach(dataSet::addSeries);

		JFreeChart chart = switchToAreaChart ? ChartFactory.createXYAreaChart(title, xAxisLabel, yAxisLabel, dataSet)
				: ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataSet);

		return chart;
	}

}
