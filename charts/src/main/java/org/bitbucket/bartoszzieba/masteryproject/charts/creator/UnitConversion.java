package org.bitbucket.bartoszzieba.masteryproject.charts.creator;

public final class UnitConversion {

	private UnitConversion() {
	}

	private static final double TO_MB_DIVISOR = Math.pow(2, 20);
	private static final double TO_SEC_DIVISOR = Math.pow(10, 3);

	public static double toMB(Number bytes) {
		return bytes.doubleValue() / TO_MB_DIVISOR;
	}

	public static double toSec(Number millis) {
		return millis.doubleValue() / TO_SEC_DIVISOR;
	}

}
