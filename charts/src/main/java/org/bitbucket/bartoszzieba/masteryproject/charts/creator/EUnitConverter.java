package org.bitbucket.bartoszzieba.masteryproject.charts.creator;

import java.util.function.Function;

public enum EUnitConverter {
	IDENTITY(x -> x.doubleValue()),
	TO_MB(UnitConversion::toMB),
	TO_SEC(UnitConversion::toSec);

	private final Function<Number, Double> converter;

	private EUnitConverter(Function<Number, Double> converter) {
		this.converter = converter;
	}

	public Function<Number, Double> getConverter() {
		return converter;
	}
}
