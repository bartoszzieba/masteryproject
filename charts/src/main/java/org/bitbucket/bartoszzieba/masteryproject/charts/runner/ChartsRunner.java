package org.bitbucket.bartoszzieba.masteryproject.charts.runner;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import org.bitbucket.bartoszzieba.masteryproject.charts.creator.ChartsCreator;
import org.bitbucket.bartoszzieba.masteryproject.charts.data.reader.ReadCsvDataWithHeader;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class ChartsRunner {

	public static void main(String[] args) throws Exception {
		ChartsRunnerArgs params = new ChartsRunnerArgs();
		CmdLineParser parser = new CmdLineParser(params);
		try {
			parser.parseArgument(args);
			new ChartsRunner().run(params);

		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			parser.printUsage(System.err);
			System.exit(1);
		}
	}

	private void run(ChartsRunnerArgs params) throws Exception {
		try (ReadCsvDataWithHeader reader = new ReadCsvDataWithHeader(params.inputFile)) {
			JFreeChart chart = new ChartsCreator().createChart(reader, params.keyConverter, params.valueConverter,
					params.title, params.xAxisLabel, params.yAxisLabel, params.switchToAreaChart);
			if (params.maxRangeYAxis > 0) {
				ValueAxis domainAxis = chart.getXYPlot().getRangeAxis();
				domainAxis.setAutoRange(false);
				domainAxis.setRange(0, params.maxRangeYAxis);
			}

			BufferedImage image = chart.createBufferedImage(1920, 1080);
			ImageIO.write(image, "png", params.outputFile.toFile());
		}
	}

}
