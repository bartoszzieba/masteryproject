package org.bitbucket.bartoszzieba.masteryproject.charts.data.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ReadCsvDataWithHeader implements AutoCloseable {

	private static final String CSV_SEPARATION_CHAR = ",";
	private final BufferedReader bufferedReader;
	private final String[] columnNames;

	public ReadCsvDataWithHeader(Path inputFile) throws IOException {
		bufferedReader = Files.newBufferedReader(inputFile);
		columnNames = bufferedReader.readLine().split(CSV_SEPARATION_CHAR);
		long distinctNames = Arrays.stream(columnNames).distinct().count();
		if (distinctNames != columnNames.length) {
			throw new RuntimeException("Collumns are not uniq");
		}
	}

	public String getKeyName() {
		return columnNames[0];
	}

	public Set<String> getSeriesNames() {
		return new HashSet<>(Arrays.asList(columnNames).subList(1, columnNames.length));
	}

	public Optional<Map<String, Long>> getNextRow() throws IOException {
		return Optional.ofNullable(bufferedReader.readLine()).map(this::parseLine);
	}

	private Map<String, Long> parseLine(String line) {
		String[] split = splitAndCheckSize(line);

		HashMap<String, Long> map = new HashMap<>();
		for (int i = 0; i < split.length; i++) {
			map.put(columnNames[i], Long.valueOf(split[i]));
		}
		return map;
	}

	private String[] splitAndCheckSize(String line) {
		String[] split = line.split(CSV_SEPARATION_CHAR);
		if (split.length != columnNames.length) {
			throw new RuntimeException(String.format("Line has another length than schema. Line: '%s'", line));
		}
		return split;
	}

	@Override
	public void close() throws Exception {
		bufferedReader.close();
	}

}
