package org.bitbucket.bartoszzieba.masteryproject.charts.runner;

import java.nio.file.Path;

import org.bitbucket.bartoszzieba.masteryproject.charts.creator.EUnitConverter;
import org.kohsuke.args4j.Option;

public class ChartsRunnerArgs {

	@Option(name = "-i", required = true, usage = "input file with csv data")
	public Path inputFile;

	@Option(name = "-o", required = true, usage = "image output file [png]")
	public Path outputFile;

	@Option(name = "-t", usage = "title of chart")
	public String title = "";

	@Option(name = "-x", usage = "x axis label")
	public String xAxisLabel = "time";

	@Option(name = "-y", usage = "y axis label")
	public String yAxisLabel = "count";

	@Option(name = "-a", usage = "use area chart")
	public boolean switchToAreaChart = false;

	@Option(name = "-k", usage = "key converter")
	public EUnitConverter keyConverter = EUnitConverter.IDENTITY;

	@Option(name = "-v", usage = "value converter")
	public EUnitConverter valueConverter = EUnitConverter.IDENTITY;

	@Option(name = "-m", usage = "max rang of yAxis")
	public long maxRangeYAxis = -1;

}
